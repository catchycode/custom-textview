package pl.catchycode.customtextview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Add TextView from ocCreate method
        CustomTextView textView = new CustomTextView(this);
        textView.setText(R.string.cats);
        LinearLayout layout = (LinearLayout)findViewById(R.id.container);
        layout.addView(textView);
        
 
    }

}
