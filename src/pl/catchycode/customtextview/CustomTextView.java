package pl.catchycode.customtextview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {

	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initFont(attrs);
	}
	
	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initFont(attrs);
	}
	
	public CustomTextView(Context context) {
		super(context);
		initFont(null);
	}

	/**
	 * Sets TextView's font.
	 * We need to check if our view isn't in edit mode before we change anything.
	 */
	private void initFont(AttributeSet attrs) {
	    if (!isInEditMode()) {
	    	
	    	String fontName = null;
	    	
	    	if (attrs!=null) {
	    		Integer textStyle = attrs.getAttributeIntValue(
	    				"http://schemas.android.com/apk/res/android", "textStyle", 
	    				Typeface.NORMAL);
	    		if (textStyle == Typeface.ITALIC) {
	    			fontName = "LinLibertine_RI.ttf";
	    		} else if (textStyle == Typeface.BOLD) {
	    			fontName = "LinLibertine_RB.ttf";
	    		} else {
	    			fontName = "LinLibertine_R.ttf";
	    		}
	    		
	    	} else {
	    		fontName = "ennobled pet.ttf";
	    	}
	    	
	        Typeface tf = Typeface.createFromAsset(
	        		getContext().getAssets(), fontName);
	        setTypeface(tf);
	    }
	}

}
