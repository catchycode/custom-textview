package pl.catchycode.customtextview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewWithAttr extends TextView {
	
	public CustomTextViewWithAttr(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initFont(attrs);
	}
	
	public CustomTextViewWithAttr(Context context, AttributeSet attrs) {
		super(context, attrs);
		initFont(attrs);
	}
	
	public CustomTextViewWithAttr(Context context) {
		super(context);
		initFont(null);
	}

	/**
	 * Sets TextView font to be the one from xml
	 */
	private void initFont(AttributeSet attrs) {
	    if (!isInEditMode()) {
	    	
	    	TypedArray attribute = getContext().obtainStyledAttributes(
	    			attrs, R.styleable.CustomTextViewWithAttr);
			String fontName = attribute.getString(
					R.styleable.CustomTextViewWithAttr_font);
			if (fontName!=null) {
				Typeface tf = Typeface.createFromAsset(
						getContext().getAssets(), fontName);
				setTypeface(tf);
			}
			attribute.recycle();
	    }
	}
}
